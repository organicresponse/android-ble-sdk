# Android BLE SDK #
This project is to be used as a library for Android applications. It's main use is to provide a communication interface with OR nodes.

### SETUP ###
1. Import the .aar file into the Android project as a dependency
2. Include these libraries in your build.gradle file since it is needed as dependencies by this library
    * androidx.appcompat:appcompat:1.1.0
    * androidx.core:core-ktx:1.2.0
    * io.reactivex.rxjava2:rxkotlin:2.4.0
    * com.squareup.retrofit2:retrofit:2.7.2
    * com.squareup.retrofit2:adapter-rxjava2:2.8.0
    * com.squareup.retrofit2:converter-scalars:2.4.0
    * com.squareup.retrofit2:converter-gson:2.1.0